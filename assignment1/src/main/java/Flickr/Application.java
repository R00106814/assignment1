package Flickr;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String args[]) {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... strings) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
      
        Detail detail = restTemplate.getForObject("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9be2e293e173d85913908be2e8ebdb5&tags=cork&safe_search=&per_page=5&page=1&format=json&nojsoncallback=1&auth_token=72157661156715160-6dc63a183e9b1f66&api_sig=a7e69a4f5f850ee1374f741512de2aec", Detail.class);
        System.out.println( detail.toString());
    }
}