package Flickr;

import java.util.List;

import ie.cit.assignment1.Movement;

public class Photos {
	
	private int page;
	private long pages;
	private int perpage;
	private long total;
	List<Photo> photo;
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public long getPages() {
		return pages;
	}
	public void setPages(long pages) {
		this.pages = pages;
	}
	public int getPerpage() {
		return perpage;
	}
	public void setPerpage(int perpage) {
		this.perpage = perpage;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<Photo> getPhoto() {
		return photo;
	}
	public void setPhoto(List<Photo> photo) {
		this.photo = photo;
	}
	
	@Override
    public String toString() {
        return "Value{" +
                "id=" + page +
                ", title='" + total + '\'' + photo +
                '}';
    }
	


}
