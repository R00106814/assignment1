package Flickr;

import java.util.List;

import ie.cit.assignment1.Movement;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Detail {
	
	
	private Photos photos;
	
	
	
	
	public Photos getPhotos() {
		return photos;
	}




	public void setPhotos(Photos photos) {
		this.photos = photos;
	}




	@Override
    public String toString() {
        return "Value{" +
                "Page=" + photos +"";
    }


}
