package Flickr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo {

	private String id;
	private String owner;
	private String secret;
	private int server;
	private int farm;
	private String title;
	private int ispublic;
	private int isfriend;
	private int isfamily;
	private String url;
	
	
	
	public String getUrl() {
		//http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
		return "http://farm" + getFarm() + ".staticflickr.com/" + getServer()+"/"+getId()+"_"+getSecret()+".jpg";
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public int getServer() {
		return server;
	}
	public void setServer(int server) {
		this.server = server;
	}
	public int getFarm() {
		return farm;
	}
	public void setFarm(int farm) {
		this.farm = farm;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getIspublic() {
		return ispublic;
	}
	public void setIspublic(int ispublic) {
		this.ispublic = ispublic;
	}
	public int getIsfriend() {
		return isfriend;
	}
	public void setIsfriend(int isfriend) {
		this.isfriend = isfriend;
	}
	public int getIsfamily() {
		return isfamily;
	}
	public void setIsfamily(int isfamily) {
		this.isfamily = isfamily;
	}
	
	@Override
    public String toString() {
        return "Value{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

	
}
