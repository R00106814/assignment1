package Flickr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import ie.cit.assignment1.Artist;

public class ImageRetriever {
	
	public Detail get(String query ,int count) {
		//http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
	    RestTemplate restTemplate = new RestTemplate();
	    
	      Detail detail = restTemplate.getForObject("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=8cb2b3ea68ca6d6853dd6f4cd05eedea&tags="+query+"&safe_search=&per_page="+count+"&page=1&format=json&nojsoncallback=1", Detail.class);
      //  System.out.println(detail.toString());
        return detail;
		
	}

}
